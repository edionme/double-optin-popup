<html>
    <head>
        <title></title>
        @include('_partials/head')
    </head>

    <body class="skin-blue">
        <div class="wrapper">
            @include('_partials/header')

            @include('_partials/sidebar')

            @yield('content')
            @include('_partials/footer')
        </div>
    </body>
        @include('_partials/foot')
</html>
